$(document).ready(function () {
  dropdown();
  tabs();
  charts();
});

function dropdown() {
  $(document).on('click', '.dropdown .dropdown_heading', function () {
    let ele = $(this);
    $('.dropdown').removeClass('show');
    ele.closest('.dropdown').toggleClass('show');
  });
  $(document).on('click', '.dropdown_item a', function () {
    let ele = $(this);
    let dropdown = ele.closest('.dropdown');
    selectedOption = ele.text();
    dropdown.find('.form_control').val(selectedOption);
    dropdown.toggleClass('show');
  });
}

function tabs() {
  $(document).on('click', '.tablist_wrapper .btn', function () {
    let ele = $(this);
    ele.closest('.tablist_wrapper').find('.btn').removeClass('selected');
    ele.addClass('selected');
    let curTabIndex = ele.index();
    ele.closest('.tabs').find('.tab_panel-wrapper .tab_panel').removeClass('show');
    ele.closest('.tabs').find(`.tab_panel-wrapper .tab_panel:nth-child(${curTabIndex + 1})`).addClass('show');
  });
}

function charts() {

  const labels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  const data = {
    labels: labels,
    datasets: [{
      label: 'Values April',
      backgroundColor: '#FF6384',
      borderColor: '#FF6384',
      data: [-38, -40, 30, 0, 33, 18, 60, -22, -6, -20, 0, 45],
    },
    {
      label: 'Values March',
      backgroundColor: '#FF9F40',
      borderColor: '#FF9F40',
      data: [-60, -40, -20, 0, 20, 40, 60, 20, -18, -5, 45, 33],
    }]
  };
  const config = {
    type: 'line',
    data: data,
    options: {
      layout: {
        padding: {
          left: 40
        }
      },
      plugins: {
        legend: {
          display: true,
          align: 'start',
          labels: {
            usePointStyle: true,
            padding: 20

          },
        }
      }
    }
  };
  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );
}


